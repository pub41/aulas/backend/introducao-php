<?php

require_once __DIR__  . '/../vendor/autoload.php';

use App\Controller\UsuarioController;
use App\Controller\HomeController;

/*
  Ir para UsuarioController - Responsta em JSON
*/
$usuario = new UsuarioController();
echo $usuario->getUsuarios();

/*
  Ir para HomeView - Renderiza HTML
*/
// $home = new HomeController();
// echo $home->home();
