<?php
use App\Controller\UsuarioController;
$listaUsuario = new UsuarioController();
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>HomeView</title>
  </head>
  <body>
        <p>Home View!</p>
        <ul>
          <?php
            $usuarios = json_decode($listaUsuario->all());
            foreach ($usuarios as $usuario) {
              echo '<li>' . $usuario->nome .' - '. $usuario->email . '</li>';
            }
          ?>
        </ul>
  </body>
</html>
