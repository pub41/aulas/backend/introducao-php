<?php

namespace App\Model;

class UsuarioModel
{

  public function all()
  {
    return json_encode([
                      ['nome' => 'João', 'email' => 'joao@joao.com'],
                      ['nome' => 'Lucas', 'email' => 'lucas@lucas.com']
                    ]);
  }

}
