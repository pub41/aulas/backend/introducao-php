<?php
namespace App\Controller;

use App\Model\UsuarioModel;
use Src\ResponseJson;

class UsuarioController extends UsuarioModel
{

  public function getUsuarios()
  {
    return ResponseJson::json($this->all());
  }

}
